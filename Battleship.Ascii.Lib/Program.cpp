#include "Program.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <unistd.h>


#include "../Battleship.GameController.Lib/GameController.h"

#pragma comment(lib,"winmm.lib")  //for MSV C++   

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */


using namespace Battleship::GameController;
using namespace Battleship::GameController::Contracts;

namespace Battleship
{
  namespace Ascii
  {
    ostream& operator<<(ostream &out, Position pos)
    {
      out << (char)('A' + pos.Column) << pos.Row;
      return out;
    }

    Program::Program()
    {
    }

    Program::~Program()
    {
    }

    list<Ship> Program::myFleet;
    list<Ship> Program::enemyFleet;
    
    void Program::Main()
    {
      cout << MAGENTA << R"(                                     |__                                       )" << endl;
      cout << R"(                                     | \ /                                     )" << endl;
      cout << R"(                                     ---                                       )" << endl;
      cout << R"(                                     / | [                                     )" << endl;
      cout << R"(                              !      | |||                                     )" << endl;
      cout << R"(                            _/|     _/|-++'                                    )" << endl;
      cout << R"(                        +  +--|    |--|--|_ |-                                 )" << endl;
      cout << R"(                     { /|__|  |/\__|  |--- |||__/                              )" << endl;
      cout << R"(                    +---------------___[}-_===_.'____                 /\       )" << endl;
      cout << R"(                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/    _  )" << endl;
      cout << R"( __..._____--==/___]_|__|_____________________________[___\==--____,------' .7 )" << endl;
      cout << R"(|                        Welcome to Battleship                         BB-61/  )" << endl;
      cout << R"( \_________________________________________________________________________|   )" << endl;
      cout << endl;
      cout << RESET;

	  InitializeGame();

      StartGame();
    }

    void Program::StartGame()
    {
      //Console::Clear();
      cout << BLUE << R"(                  __     )" << endl;
      cout << R"(                 /  \    )" << endl;
      cout << R"(           .-.  |    |   )" << endl;
      cout << R"(   *    _.-'  \  \__/    )" << endl;
      cout << R"(    \.-'       \         )" << endl;
      cout << R"(   /          _/         )" << endl;
      cout << R"(  |      _  /""          )" << endl;
      cout << R"(  |     /_\'             )" << endl;
      cout << R"(   \    \_/              )" << endl;
      cout << R"(    """"""""             )" << RESET << endl;

      do
      {
        cout << endl;
        cout << BOLDCYAN << R"(Player, it's your turn   )" << RESET << endl;

		bool PositionValid = false;
		string input;
		Position position;
		
		cout << CYAN << R"(Enter coordinates for your shot in range [A1 - H8]:   )" << endl;

    while(1)
    {
      getline(cin, input);
      position = ParsePosition(input);
      if( ((position.Column >= 0) && (position.Column < 8)) && ((position.Row >= 1) && (position.Row < 9)))
        {
            break;
        }
        cout << "The position is outside the playing field, repeat the shot:"<<endl;
    }
		

        bool isHit = GameController::GameController::CheckIsHit(enemyFleet, position);
        if (isHit)
        {
          GameController::GameController::markPosition(enemyFleet,position);

         
            // Console::Beep();

			cout << YELLOW << R"(                \         .  ./         )" << endl;
            cout << R"(              \      .:"";'.:..""   /   )" << endl;
            cout << R"(                  (M^^.^~~:.'"").       )" << endl;
            cout << R"(            -   (/  .    . . \ \)  -    )" << endl;
            cout << R"(               ((| :. ~ ^  :. .|))      )" << endl;
            cout << R"(            -   (\- |  \ /  |  /)  -    )" << endl;
            cout << R"(                 -\  \     /  /-        )" << endl;
            cout << R"(                   \  \   /  /          )" << endl;
			cout << GREEN << ("Yeah ! Nice hit !") << RESET << endl;

        if (GameController::GameController::CheckIfAllShipsSunk(enemyFleet)){
            cout <<endl;
            cout << R"(Game Over You wins)" << endl;
            break;
          }
		}
		else
		{
			cout << RED << ("Miss") << RESET << endl;
		}

	cout << "____________________________________________" << endl;
	sleep(1);
	

        position = GetRandomPosition();
        isHit = GameController::GameController::CheckIsHit(myFleet, position);
        cout << endl;

        if (isHit)
        {
            //Console::Beep();

            GameController::GameController::markPosition(enemyFleet,position);

			cout << YELLOW << R"(                \         .  ./         )" << endl;
            cout << R"(              \      .:"";'.:..""   /   )" << endl;
            cout << R"(                  (M^^.^~~:.'"").       )" << endl;
            cout << R"(            -   (/  .    . . \ \)  -    )" << endl;
            cout << R"(               ((| :. ~ ^  :. .|))      )" << endl;
            cout << R"(            -   (\- |  \ /  |  /)  -    )" << endl;
            cout << R"(                 -\  \     /  /-        )" << endl;
            cout << R"(                   \  \   /  /          )" << endl;

			cout << RED << "(Computer shoot in " << position << " and " << "hit your ship !)" << RESET << endl;

  if (GameController::GameController::CheckIfAllShipsSunk(enemyFleet)) {
            cout <<endl;
            cout << R"(Game Over PC wins)" << endl;
            break;
          }
        }
		else
		{
			cout << GREEN << "(Computer shoot in " << position << " and missed )   " << RESET << endl;
		}

	    cout << "============================================" << endl;
      }
      while (true);
    }

	Position Program::ParsePosition(string input)
    {
      Position outPosition;
      const char error = -1;
      
      if((input.length() != 2))
      {
        outPosition.Column = (Letters)Letters::INVALID_LETTER;
        outPosition.Row = error - '0';
        return outPosition;
      }

      char cColumn = toupper(input.at(0));
      char cRow = input.at(1);
      
      if((input.at(1) == '0') || (input.at(1) == '9')) 
      {
        cRow = error;
      }
	    int nColumn = (cColumn - 'A');
      Letters lColumn = (Letters)nColumn;
      if(lColumn > Letters::H)
      {
        lColumn = (Letters)Letters::INVALID_LETTER;
      }

      int nRow = cRow - '0';

      outPosition.Column = lColumn;
      outPosition.Row = nRow;
      return outPosition;
    }

    Position Program::GetRandomPosition()
    {
      const int size = 8;
      srand((unsigned int) time(NULL));
      Letters lColumn = (Letters)(rand() % size);
      int nRow = (rand() % size);

      Position position(lColumn, nRow);
      return position;
    }

    void Program::InitializeGame()
    {
      InitializeMyFleet();

      InitializeEnemyFleet(enemyFleet);
    }

	void Program::InitializeMyFleet()
	{
		myFleet = GameController::GameController::InitializeShips();

		cout << YELLOW << "Please position your fleet (Game board has size from A to H and 1 to 8) :" << RESET << endl;
		for_each(myFleet.begin(), myFleet.end(), [](Ship &ship)
		{
			cout << endl;
			cout << BOLDYELLOW << "Please enter the positions for the " << ship.Name << " (size: " << ship.Size << ")" << RESET << endl;
			for (int i = 1; i <= ship.Size; i++)
			{
        cout << BOLDGREEN << "Enter position [A1 - H8]:" << i << " of " << ship.Size << RESET << "\n";
				string input;
      Position inputPosition;
      while(1)
      {
        getline(cin, input);
        inputPosition = ParsePosition(input);
        if( ((inputPosition.Column >= 0) && (inputPosition.Column < 8)) && ((inputPosition.Row >= 1) && (inputPosition.Row < 9)))
          {
              break;
          }
          cout << BOLDRED << "The position is outside the playing field, repeat the placement:" <<endl;
          cout << BOLDGREEN << "Enter position [A1 - H8]:" << i << " of " << ship.Size << RESET << "\n";
      }
				
      ship.AddPosition(inputPosition);
			}
		});
	}

	void Program::InitializeEnemyFleet(list<Ship>& Fleet)
	{
		Fleet = GameController::GameController::InitializeShips();

		for_each(Fleet.begin(), Fleet.end(), [](Ship& ship)
		{
			if (ship.Name == "Aircraft Carrier")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 4));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 6));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 7));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 8));
			}
			if (ship.Name == "Battleship")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 6));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 7));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 8));
			}
			if (ship.Name == "Submarine")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 3));
			}
			if (ship.Name == "Destroyer")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::F, 8));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::G, 8));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::H, 8));
			}
			if (ship.Name == "Patrol Boat")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 6));
			}
		});
	}
  }
}