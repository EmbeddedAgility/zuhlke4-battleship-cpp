#include "ParsePositionTests.h"

#include <stdlib.h>
#include <algorithm>
#include <stdexcept>
#include <iostream>

CPPUNIT_TEST_SUITE_REGISTRATION (ParsePositionTests);

void ParsePositionTests :: ValidPositionTest(void)
{      
	string input = "a1";
	Position expected(Letters::A, 1);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

void ParsePositionTests :: InvalidPositionTestRow(void)
{      
	string input = "h21";
	Position expected(Letters::INVALID_LETTER, -49);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

void ParsePositionTests :: InvalidPositionTestColumn(void)
{      
	string input = "o7";
	Position expected(Letters::INVALID_LETTER, 7);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

void ParsePositionTests :: InvalidPositionTestColumnRow(void)
{      
	string input = "o55";
	Position expected(Letters::INVALID_LETTER, -49);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

void ParsePositionTests :: InvalidPositionTestShortInput(void)
{      
	string input = "1";
	Position expected(Letters::INVALID_LETTER, -49);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

void ParsePositionTests :: InvalidPositionTestShipPlacementNum(void)
{      
	string input = "1";
	Position expected(Letters::INVALID_LETTER, -49);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

void ParsePositionTests :: InvalidPositionTestShipPlacementRandom(void)
{      
	string input = "asldmalsdjlaksdas";
	Position expected(Letters::INVALID_LETTER, -49);
	Position result = Program::ParsePosition(input);
            
    CPPUNIT_ASSERT_EQUAL(expected.Column, result.Column);
    CPPUNIT_ASSERT_EQUAL(expected.Row, result.Row);
}

