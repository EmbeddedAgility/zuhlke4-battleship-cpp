#pragma once

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../Battleship.Ascii.Lib/Program.h"
#include "../Battleship.GameController.Lib/GameController.h"

using namespace std;
using namespace Battleship::Ascii;

class ParsePositionTests : public CPPUNIT_NS :: TestFixture
{
    CPPUNIT_TEST_SUITE (ParsePositionTests);
    CPPUNIT_TEST (ValidPositionTest);
    CPPUNIT_TEST (InvalidPositionTestRow);
    CPPUNIT_TEST(InvalidPositionTestColumn);
    CPPUNIT_TEST(InvalidPositionTestColumnRow);
    CPPUNIT_TEST(InvalidPositionTestShortInput);
    CPPUNIT_TEST(InvalidPositionTestShipPlacementNum);
    CPPUNIT_TEST(InvalidPositionTestShipPlacementRandom);
    CPPUNIT_TEST_SUITE_END ();

    protected:
        void ValidPositionTest (void);
        void InvalidPositionTestRow (void);
        void InvalidPositionTestColumn (void);
        void InvalidPositionTestColumnRow (void);
        void InvalidPositionTestShortInput (void);
        void InvalidPositionTestShipPlacementNum (void);
        void InvalidPositionTestShipPlacementRandom (void);
};