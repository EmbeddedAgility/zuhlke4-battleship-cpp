#include "Position.h"

namespace Battleship
{
  namespace GameController
  {
    namespace Contracts
    {
      Position::Position(Letters column, int row) : Column(column), Row(row), PositionHit(false)
      {
      }

      Position::Position(const Position &init) : Column(init.Column), Row(init.Row), PositionHit(false)
      {
      }

      Position::~Position()
      {
      }

      Position &Position::operator=(const Position &rhs)
      {
        Column = rhs.Column;
        Row = rhs.Row;

        return *this;
      }

      bool Position::operator==(const Position &rhs) const
      {
        return (Column == rhs.Column) && (Row == rhs.Row);
      }

      void Position::MarkPosition(){
        PositionHit = true;
      }

      bool Position::IsPositionMarked(){
        return PositionHit;
      }
    }
  }
}

